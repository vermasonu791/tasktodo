import { Component, OnInit } from '@angular/core';
import { TasktodoService } from '../tasktodo.service';
import { NgForm } from '@angular/forms';
import { Username } from '../user';
import { FlashMessagesService } from 'angular2-flash-messages';

@Component({
  selector: 'app-user-login',
  templateUrl: './user-login.component.html',
  styleUrls: ['./user-login.component.css']
})
export class UserLoginComponent implements OnInit {
  public users = [];
  flag: Boolean = false;
  public error:String;
  constructor(private task: TasktodoService,private _flashMessagesService: FlashMessagesService) { }

  ngOnInit() {
    
document.getElementById('logout').style.display="none";
  }
  onSubmit(form: NgForm) {
    var username = form.value.name;
    var password = form.value.password;
   
    var isLogin = false;
    for (var key in localStorage) {
      if (username == key && password == localStorage.getItem(key)) {
        isLogin = true;
        this._flashMessagesService.show('Success! login', { cssClass: 'flash', timeout: 1000 });
        this.task.setLoggin();
        document.getElementById("login").style.display="none";
        document.getElementById("register").style.display="none";  
        document.getElementById('logout').style.display="block";
        this.task.getrouter().navigate(['/addtask']);
        break;
      }
    }

    if(isLogin == false){
      this.error="Username or password incorrect";
    }

  }
  showlogin(){
   
    document.getElementById("login").style.display="block";
    this._flashMessagesService.show('Logout!', { cssClass: 'flash', timeout: 1000 });
    document.getElementById("register").style.display="block";  
    
  }
}
