import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TasktodoService } from '../tasktodo.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  
  constructor(private task: TasktodoService) { 
 
  }

  ngOnInit() {
   
  }

}
