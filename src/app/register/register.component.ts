import { Component, OnInit } from '@angular/core';
import { TasktodoService } from '../tasktodo.service';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { FlashMessagesService } from 'angular2-flash-messages';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  constructor(private task: TasktodoService,private flashMessagesService: FlashMessagesService) { }

  ngOnInit() {


  }
  onsubmit(form: NgForm) {
    var name = form.value.name;
    console.log(name);
    localStorage.setItem(name, form.value.password);
    this.flashMessagesService.show('Register!', { cssClass: 'flash', timeout: 1000 });

  }
}
