import { TestBed } from '@angular/core/testing';

import { TasktodoService } from './tasktodo.service';

describe('TasktodoService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TasktodoService = TestBed.get(TasktodoService);
    expect(service).toBeTruthy();
  });
});
