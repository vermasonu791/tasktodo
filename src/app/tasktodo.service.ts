import { Injectable } from '@angular/core';
import { Username } from './user';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class TasktodoService {
  private loggedin: boolean = false;
  constructor(public router: Router) { }

  setLoggin() {
    this.loggedin = true;
    
  }
  getrouter(): Router {
    return this.router;
  }
  isLoggedin() {
    return this.loggedin;
  }
  
}
