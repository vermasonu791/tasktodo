import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { TasktodoService } from '../tasktodo.service';
@Component({
  selector: 'app-task-add',
  templateUrl: './task-add.component.html',
  styleUrls: ['./task-add.component.css']
})
export class TaskAddComponent implements OnInit {
  public title: any;
  public url: any;
  public link: any;
  public show: boolean = false;
  public Edit: any = 'Show';
  public showtask = false;
  constructor(private task:TasktodoService) { }

  ngOnInit() {
    if (!this.task.isLoggedin()) {
      this.task.getrouter().navigate(['/login']);
    }

  }
  showinput() {
    this.show = !this.show;
    if (this.show)
      this.Edit = "Hide";
    else
      this.Edit = "Show";
  }
  getimage(event: any) {
    if (event.target.files && event.target.files[0]) {
      var reader = new FileReader();

      reader.onload = (event: ProgressEvent) => {
        this.url = (<FileReader>event.target).result;
        console.log(this.url);
      }

      reader.readAsDataURL(event.target.files[0]);
    }

  }
  submit(form: NgForm) {
    this.showtask = true;
    this.link = form.value.link;
    this.title = form.value.title;
    // Your existing code unmodified...
    var iDiv = document.createElement('div');
    iDiv.id = 'show-item';
    iDiv.className = 'container';
    document.getElementsByTagName('body')[0].appendChild(iDiv);

    // Now create and append to iDiv
   
    var img = document.createElement('img');
    img.src = this.url;
    img.height = 200;
    img.width = 200;
    img.className = 'image';
    var h3 = document.createElement('h3');
    h3.innerHTML = this.title;
    h3.className = 'block-2';
    var linkk = document.createElement('a');
    linkk.setAttribute("href", this.link);
    linkk.innerHTML = this.link;
    h3.className = 'link';
    var br = document.createElement('br');
    // The variable iDiv is still good... Just append to it.
    
    iDiv.appendChild(img);
    iDiv.appendChild(br);
    iDiv.appendChild(h3);
    iDiv.appendChild(br);
    iDiv.appendChild(linkk);
    // Now create and append to iDiv

    // The variable iDiv is still good... Just append to it.

  }

}
